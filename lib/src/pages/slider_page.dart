import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {

  double _valorSlider = 150.0;
  bool _valorCheckbox = false;
  Color _colorActive = Colors.amberAccent;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sliders"),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 50.0),
        child: Column(
          children: <Widget>[
            _crearSlider(),
            _crearCheckbox(),
            _crearSwitch(),
            Expanded(
                child: _crearImagen()
            )
          ],
        ),
      ),
    );
  }

  Widget _crearSlider() {
    return Slider(
      activeColor: _colorActive,
      label: 'Tamaño de la imagen',
      value: _valorSlider,
      min: 100.0,
      max: 400.0,
      onChanged: (_valorCheckbox) ? null : (valor) {
        setState(() {
          _valorSlider = valor;
        });
      },
    );
  }

  Widget _crearImagen() {
    return Image(
        width: _valorSlider,
        image: NetworkImage(
            "https://images-na.ssl-images-amazon.com/images/I/71vntClRfjL._AC_SL1500_.jpg"),
        fit: BoxFit.contain);
  }

  Widget _crearCheckbox() {
    /*return Checkbox(
        value: _valorCheckbox,
        onChanged: (valor) {
          setState(() {
            _valorCheckbox = valor;
          });
        });*/

    return CheckboxListTile(
        activeColor: _colorActive,
        title: Text("Bloquear slider"),
        value: _valorCheckbox,
        onChanged: (valor) {
          setState(() {
            _valorCheckbox = valor;
          });
        });
  }

  Widget _crearSwitch() {
    return SwitchListTile(
        activeColor: _colorActive,
        title: Text("Bloquear slider"),
        value: _valorCheckbox,
        onChanged: (valor) {
          setState(() {
            _valorCheckbox = valor;
          });
        });
  }
}
