import 'dart:math';

import 'package:flutter/material.dart';

class AnimatedContainerPage extends StatefulWidget {
  @override
  _AnimatedContainerPageState createState() => _AnimatedContainerPageState();
}

class _AnimatedContainerPageState extends State<AnimatedContainerPage> {

  double _width = 100.0;
  double _height = 100.0;
  Color _color = Colors.blue;

  BorderRadiusGeometry _borderRadius = BorderRadius.circular(4.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Animated Container"),
      ),
      body: Center(
        child: AnimatedContainer(
          duration: Duration(milliseconds: 500),
          curve: Curves.easeInCubic,
          width: _width,
          height: _height,
          decoration: BoxDecoration(
            borderRadius: _borderRadius,
            color: _color,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_arrow),
        onPressed: _changeConatiner,
      ),
    );
  }

  _changeConatiner() {

    final random = Random();

    _width = random.nextInt(350).toDouble();
    _height = random.nextInt(600).toDouble();
    _color = Color.fromRGBO(
      random.nextInt(255),
        random.nextInt(255),
        random.nextInt(255),
        1
    );

    _borderRadius = BorderRadius.circular(random.nextInt(100).toDouble());

    setState(() {

    });
  }
}
