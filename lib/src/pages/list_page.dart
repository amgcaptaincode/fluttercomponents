import 'dart:async';

import 'package:flutter/material.dart';

class ListPage extends StatefulWidget {
  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {

  List<int> _listaNumeros = new List();
  int _ultimoItem = 0;
  bool _isLoading = false;

  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();

    _agregar10();

    _scrollController.addListener(() {

      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        //_agregar10();
        _fetchData();
      }

    });

  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Listas"),
      ),
      body: Stack(
        children: <Widget>[
          _crearRefreshIndicator(),
          _crearLoading(),
        ],
      )
    );
  }

  Widget _crearRefreshIndicator() {
    return RefreshIndicator(
        child: _crearLista(),
        onRefresh: obtenerPagina1
    );
  }

  Widget _crearLista() {

    return ListView.builder(
      controller: _scrollController,
      itemCount: _listaNumeros.length,
        itemBuilder: (context, index) {

        final imagen = _listaNumeros[index];

          return FadeInImage(
            height: 250.0,
            placeholder: AssetImage("assets/jar-loading.gif"),
            image: NetworkImage("https://picsum.photos/id/$imagen/500/300"),
            fit: BoxFit.cover,
          );
      },
    );

  }

  Future<Null> obtenerPagina1() async {

    final duration = new Duration(seconds:  2);
    new Timer(duration, (){
      _listaNumeros.clear();
      _ultimoItem++;
      _agregar10();
    });

    return Future.delayed(duration);
  }

  void _agregar10() {
    for (var i = 1; i <= 10; i++) {
      _listaNumeros.add(_ultimoItem++);
    }

    setState(() {});
  }

  Future<Null> _fetchData() async {
    _isLoading = true;
    setState(() {});

    final duration = new Duration(seconds: 2);

    return new Timer(duration, _respuesta);
  }

  void _respuesta() {

    _isLoading = false;

    _scrollController.animateTo(
        _scrollController.position.pixels + 80,
        duration: new Duration(milliseconds: 250),
        curve: Curves.fastOutSlowIn
    );

    _agregar10();

  }

  Widget _crearLoading() {
    if (_isLoading) {
      return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(),
            ],
          ),
          SizedBox(height: 20.0,)
        ],
      );
    } else {
      return Container();
    }
  }
}
