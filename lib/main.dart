

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:componentes/src/routes/routes.dart';

import 'package:componentes/src/pages/alert_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],

      supportedLocales: [
        const Locale('en', ''), // English, no country code
        const Locale('es', 'MX'),
      ],

      title: 'Componentes APP',
      //home: HomePage()
      initialRoute: "/",
      routes: getAppRoutes(),
      onGenerateRoute: (RouteSettings settings) {
        print("Ruta llamada ${settings.name}");
          return MaterialPageRoute(
              builder: (BuildContext context) => AlertPage());
      },
    );
  }
}

